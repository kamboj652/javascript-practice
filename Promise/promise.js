//https://youtu.be/Deq4kt4SmXw
const promesa = Notification.requestPermission();
const promesa2 = promesa
    .then(function(param){
        if(param === "granted"){
            console.log("Nos ha dicho que si");
            return true;
        }else{
            console.log("Nos ha dicho que no");
            return false;
        }        
    })
    .catch(function(err){
        console.error(err);
    })
    .finally(function(){
        console.log("Ya estaría");
    });

promesa2.then(function(r){

});


//fetch
fetch("/api/data").then(response => {
    console.log("Termina con estado "+ response.status);
    return response.json();
}).then(body => {
    console.log(body);
})
.catch(e => console.error(e));