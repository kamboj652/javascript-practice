//https://youtu.be/Deq4kt4SmXw
const promesa = new Promise(function(resolve, reject){
    console.log("Me ejecuto asíncronamente");
    setTimeout(() => {
        resolve(42);    
    }, 5000);
    reject(new Error("No me apetece trabajar"));
});

promesa.then((res) => console.log("Termina mi promesa con " + res)).catch((err)=>{ console.error(err.message)});

console.log("Estoy al final de mi programa");