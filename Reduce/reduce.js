//https://youtu.be/Deq4kt4SmXw
const flores = ["Margarita", "Amapola", "Petunia", "Geranio"];

const final = flores.reduce((ac, item, i) =>{
    console.log({ac, item});
    console.log(item, "tiene", item.length , "letras");
    return ac + item.length;
}, 0);

console.log({final});


const tareas = [
    {nombre: "Regar las plantas" , prioridad: "A"},
    {nombre: "Comprar el pan" , prioridad: "B"},
    {nombre: "Enviar e-mails" , prioridad: "A"},
    {nombre: "Poner la lavadora" , prioridad: "C"},
    {nombre: "Cargar el móvil" , prioridad: "B"},
    {nombre: "Limpiar la cocina" , prioridad: "A"},
];


const finalObj = tareas.reduce((obj, tarea)=> {
    console.log({tarea});
    if(!obj[tarea.prioridad]){
        obj[tarea.prioridad] = [];
    }
    obj[tarea.prioridad].push(tarea.nombre);
    return obj;
},{});

console.log({finalObj});



/*
{
    "A": ["..","..",".."],
    "B": ["..",".."],
    "C": [".."],
}
*/