// https://youtu.be/2Nt-ZrNP22A

const http = require('http');
const WebSocketServer = require("websocket").server;
let connection = null;

const httpserver = http.createServer((req,res) => {
    console.log("we have received a request");
});


const websocket = new WebSocketServer({
    "httpServer": httpserver
});

websocket.on("request", request => {
    connection = request.accept(null, request.origin);
    connection.on("open", () => connection.log("opened!!"));
    connection.on("close", () => connection.log("closed!!"));
    connection.on("message",  message => {
        console.log(`Received message ${message.utf8Data}`);
    });
    
    //sendEvery5Seconds();
   
});

httpserver.listen(8080, () => console.log("My server is listening on port 8080"));

function sendEvery5Seconds(){
    connection.send(`Message ${Math.random()}`);
    setTimeout(sendEvery5Seconds, 5000);
}


//Client code
/*
ws = new WebSocket("ws://localhost:8080");
ws.onmessage = message => console.log(`We received a message ${message.data}`);
ws.send('Hello server from client');


*/
